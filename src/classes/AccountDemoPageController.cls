public class AccountDemoPageController{
    public List<Account> accounts {get;set;}
    
    public AccountDemoPageController(){
        accounts = new List<Account>();
        accounts = AccountService.getAccounts();
    }
}